# How to use this repository to install MIIMETIQ LITE using Rancher

## 0. Requirements

* A machine powerful enough (2 cores and 4GB of memory is recommended) to run Rancher and MIIMETIQ LITE
* An operating system that supports Docker (Ubuntu 16.04 64 bit is recommended)
* A license issued to you by Nexiona
* Credentials provided from Nexiona to access Nexiona docker registry

## 1. Install Docker

Install a recent version of Docker using your package manager or follow the instructions in the Docker website (https://docs.docker.com/engine/installation/). If you are using Ubuntu 16.04, this is as simple as doing this:

```
sudo apt-get install docker.io
```

## 2. Install Rancher

Again in the terminal, install Rancher latest stable release executing the following command:

```
sudo docker run -d --restart=unless-stopped -p 8080:8080 rancher/server:stable

```
   
## 3. Configure Rancher

### 3.1 Add a new host

Open your web browser and go to the port 8080 of the machine where you installed rancher (e.g. http://192.168.1.2:8080) and add a new host under **Infrastructure > Hosts** (it can be the same host as the Rancher server). 

When you are asked the *Host Registration URL*, use the same URL that you are using to access Rancher (e.g. http://192.168.1.2:8080).

**If you are using the same machine for Rancher and the host, in step 4 use the same LAN address as before (e.g. 192.168.1.2).**

Finally, copy the command and paste it in the terminal of the host machine.

### 3.2 Add Nexiona's private docker registry

In **Infrastructure > Registries** add a custom registry using the address _registry.nexiona.io_ and the credentials provided by Nexiona.

### 3.3 Add a new catalog

In **Admin > Settings** mark as disabled the included catalogs (Rancher and Community) and add a custom one with the following attributes:

```
Name:    Nexiona
URL:     https://bitbucket.org/m2mcloudfactory/rancher-catalog
Branch:  master
```

### 3.4 Add credentials in Rancher (optional)

Consider securing your Rancher installation by adding a username/password under **Admin > Accounts**.

## 4. Install MIIMETIQ LITE

Browse Rancher catalog and view details of MIIMETIQ LITE. Fill in all the empty configuration options with the provided license data and launch it.

Mandatory options:

* *Customer name*: Enter your name here
* *ProductID*: ProductID as provided in your license
* *Manager Username*: Username as provided in your license
* *Manager Password*: Password as provided in your license

If this the first time MIIMETIQ LITE is deployed, Rancher needs to download the Docker image which is around 2 GB in size and it may take some time. Wait until all the containers are active and if after some time some of the containers keep flipping their state, restart the *uwsgi* container.

## 5. Activate your license

After the deployment is done, contact Nexiona support to get your license activated.

Once your license has been activated, MIIMETIQ LITE will be accesible using the URL previously configured (add it to your hosts file if it's not resolvable by your browser).
