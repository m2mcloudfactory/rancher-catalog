# MIIMETIQ IoT COMPOSER

<span style="color:red">**This is an alpha version and it is not recommended for production. Use it at your own risk.**</span>

MIIMETIQ® IoT COMPOSER provides everything needed for any M2M/IoT Solution: Built-in Asset Management, Identity Management, License Management and AEN (Alarms, Events and Notification Manager) with the ability to integrate with external business systems and data sets. It allows to integrate with any sensor or device communication protocol as well as with any third-party business software or data set.

### Deployment
* Select MIIMETIQ IoT COMPOSER from the Nexiona catalog
* Answer all the questions
* Click the launch button

### Usage
* MIIMETIQ® IoT COMPOSER's Control panel is available on port 80. The administrator user is "amAdmin" and the password will be defined in one of the following questions.

## CHANGELOG

### 2.0.0-alpha4

#### New Features
- [DS] [InfluxDB] Added built-in support for InfluxDB as metrics database. New rancher questions let you select between Graphite, InfluxDB, use both at the same time, or none.

#### Changes
- [Deployment] [WARNING: Rancher must be upgraded manually before upgrading the stack!] Upgraded Rancher to 1.6.18. This solves several bugs and increases the list of supported versions of Docker (see new version of installation docs) 

#### Fixes
- [Historics] Fixed bug that made the service crash when processing signals of an already deleted asset. Now, signals of a deleted asset that are pending to be historized will be stored, using the last historic record as refere
