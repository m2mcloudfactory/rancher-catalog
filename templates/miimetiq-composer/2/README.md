# MIIMETIQ IoT COMPOSER

<span style="color:red">**This is an alpha version and it is not recommended for production. Use it at your own risk.**</span>

MIIMETIQ® IoT COMPOSER provides everything needed for any M2M/IoT Solution: Built-in Asset Management, Identity Management, License Management and AEN (Alarms, Events and Notification Manager) with the ability to integrate with external business systems and data sets. It allows to integrate with any sensor or device communication protocol as well as with any third-party business software or data set.

## Deployment
* Select MIIMETIQ IoT COMPOSER from the Nexiona catalog
* Answer all the questions
* Click the launch button

## Usage
* MIIMETIQ® IoT COMPOSER's Control panel is available on port 80. The administrator user is "amAdmin" and the password will be defined in one of the following questions.

## CHANGELOG

### 2.0.0-alpha2
#### New Features
#### Changes
#### Fixes
- [IDM] Fixed error 500 when updating groups 
- [Assets Authnorization Manager] Fixed missing bindings when using related-assets relay
- [MQTT-CL] Fixed integration of MQTT Connection Layer with the new IDM asset authorization backend

