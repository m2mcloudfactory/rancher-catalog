version: '2'
services:
  database:
    image: mongo:3.6.3
    volumes:
      - database-data:/var/lib/mongodb
    restart: always
    command: /usr/bin/mongod --bind_ip_all --dbpath /var/lib/mongodb
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:host_label_soft: io.nexiona.host.type=dedicated

  email-relay:
    image: namshi/smtp
    environment:
      SMARTHOST_ADDRESS: $SMTP_ADDRESS
      SMARTHOST_PORT: $SMTP_PORT
      SMARTHOST_USER: $SMTP_USER
      SMARTHOST_PASSWORD: $SMTP_PASSWORD
      SMARTHOST_ALIASES: "*"
    restart: always
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  message-broker:
    image: nexiona/miimetiq-message-broker:2.0.0-alpha8
    environment:
      RABBITMQ_SERVER_START_ARGS: "-eval error_logger:tty(true)"
      RABBITMQ_EXTRA_PLUGINS: $RABBITMQ_EXTRA_PLUGINS
    ports:
      - "5672:5672"
    volumes:
      - message-broker-data:/var/lib/rabbitmq
    restart: always
    command: wait-for-config.sh /usr/local/bin/rabbitmq-wrapper.sh
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:host_label_soft: io.nexiona.host.type=dedicated
    metadata:
      ADMIN_PASSWORD: '${SERVICE_ACCOUNT_PASSWORD}'

  API-core:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    environment:
      UWSGI_SOCKET: ":7000"
      UWSGI_ENABLE_THREADS: "1"
      UWSGI_WORKERS: "4"
      UWSGI_THREADS: "4"
      UWSGI_BUFFER_SIZE: "${API_MAX_REQUEST_HEADER_SIZE}"
      UWSGI_DIE_ON_TERM: "1"
      UWSGI_MASTER: "1"
      UWSGI_NEED_APP: "1"
    volumes:
      - miimetiq-crt:/opt/miimetiq/data
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh wait-for-it.sh message-broker:5672 --timeout=30 --strict -- uwsgi --wsgi-file /opt/miimetiq/bin/uwsgi.py --pidfile /var/run/uwsgi.pid 
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: &miimetiq-conf
      IDM_PASSWORD: '${IDM_PASSWORD}'
      SERVICE_ACCOUNT_PASSWORD: '${SERVICE_ACCOUNT_PASSWORD}'
      CUSTOMER_NAME: '${CUSTOMER_NAME}'
      INFLUXDB_PASSWORD: '${INFLUXDB_ADMIN_ACCOUNT_PASSWORD}'
      MIIMETIQ_MANAGER_AMQP_HOST: '${MIIMETIQ_MANAGER_HOST}'
      MIIMETIQ_MANAGER_AMQP_USER: '${MIIMETIQ_MANAGER_USER}'
      MIIMETIQ_MANAGER_AMQP_PASSWORD: '${MIIMETIQ_MANAGER_PASSWORD}'
      MIIMETIQ_MANAGER_PRODUCT_ID: '${PRODUCT_ID}'
      AMQP_PASSWORD: '${SERVICE_ACCOUNT_PASSWORD}'
      LOG_LEVEL: '${MIIMETIQ_LOG_LEVEL}'
      SMTP_SENDER: '${SMTP_SENDER}'
      AMQP_PUBLIC_HOST: 'amqp.${MIIMETIQ_URL}'
      MQTT_PUBLIC_HOST: 'mqtt.${MIIMETIQ_URL}'
      DEVICE_AUTH_BACKEND: '${DEVICE_AUTH_BACKEND}'
      DEVICE_AUTH_HASH_SEED: '${DEVICE_AUTH_HASH_SEED}'
      AMQP_AUTHZ_RELAY_TO_RELATED_ASSETS: '${AMQP_AUTHZ_RELAY_TO_RELATED_ASSETS}'
      PAGINATION_LIMIT: '${PAGINATION_LIMIT}'
      PAGINATION_DEFAULT: '${PAGINATION_DEFAULT}'
      CUSTOM_RPCS: '${CUSTOM_RPCS}'
      CUSTOM_RPCS_AUTHENTICATED: '${CUSTOM_RPCS_AUTHENTICATED}'
      IDM_MIIMETIQ_SECRET_KEY: '${IDM_MIIMETIQ_SECRET_KEY}'
      
  webserver:
    image: nexiona/miimetiq-webserver:2.0.0-alpha8
    ports:
      - "80:80"
      - "443:443"
    restart: always
    command: wait-for-config.sh /usr/sbin/nginx
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata:
      nginx-conf:
        API_SERVER_NAME: 'api.${MIIMETIQ_URL}'
        PANEL_SERVER_NAME: 'panel.${MIIMETIQ_URL}'
        USE_SSL: '${USE_SSL}'
        GZIP_COMPRESSION: '${GZIP_COMPRESSION}'
        API_TIMEOUT: '${API_TIMEOUT}'
        API_MAX_BODY_SIZE: '${API_MAX_BODY_SIZE}'
      ssl-store:
        SSL_CERT: '${SSL_CERT}'
        SSL_PRIVKEY: '${SSL_PRIVKEY}'

  MQTT-server:
    image: nexiona/miimetiq-mqtt-server:2.0.0-alpha8
    environment:
      MQTT_USE_TLS: $MQTT_USE_TLS
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    ports:
      - "1883:1883"
      - "8883:8883"
    restart: always
    command: wait-for-config.sh /usr/bin/node /opt/miimetiq/git/utils/mqtt_cl/mqtt_cl.js
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: 
      <<: *miimetiq-conf
      MQTT_TLS_CERT: '${MQTT_TLS_CERT}'
      MQTT_TLS_KEY: '${MQTT_TLS_KEY}'

  rules-engine:
    image: nexiona/miimetiq-rules-engine:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git/miimetiq/workers/signals/models/rules_backend
    command: wait-for-config.sh node rules_worker.js
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  readers-injector:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.ds.reader.injector -n miimetiq.workers.ds.reader.injector.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  {{- if eq .Values.INCLUDE_GRAPHITE_SERVICES "true" }}
  graphite-database:
    image: nexiona/miimetiq-graphite-database:2.0.0-alpha8
    volumes:
      - graphite-database-data:/opt/graphite/storage/whisper
    restart: always
    working_dir: /opt/graphite
    command: wait-for-config.sh /usr/local/bin/start-carbon.sh --debug
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata:
      RETENTIONS: '${CARBON_RETENTION_POLICY}'
      AMQP_ADMIN_PASSWORD: '${SERVICE_ACCOUNT_PASSWORD}'
      GRAPHITE_STORAGE_AGGREGATION: '${GRAPHITE_STORAGE_AGGREGATION}'

  graphite-metrics-injector:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.signals.ds.readers.graphite_injector -n miimetiq.workers.signals.ds.readers.graphite_injector.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  graphite-metrics-readers-api:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
      - graphite-database-data:/opt/miimetiq/env/opt/graphite/storage/whisper
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.services.graphite.metrics_proxy -n miimetiq.workers.services.graphite.metrics_proxy.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf
  {{- end }}

  {{- if eq .Values.INCLUDE_INFLUX_SERVICES "true" }}
  influx-database:
    image: influxdb:1.5.4-alpine
    environment:
      INFLUXDB_DB: miimetiq
      INFLUXDB_HTTP_AUTH_ENABLED: true
      INFLUXDB_ADMIN_USER: admin
      INFLUXDB_ADMIN_PASSWORD: $INFLUXDB_ADMIN_ACCOUNT_PASSWORD
      INFLUXDB_REPORTING_DISABLED: true
      INFLUXDB_LOGGING_LEVEL: $MIIMETIQ_LOG_LEVEL
    volumes:
      - influx-database-data:/var/lib/influxdb
    restart: always
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:host_label_soft: io.nexiona.host.type=dedicated

  influx-metrics-injector:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.signals.ds.readers.influx_injector -n miimetiq.workers.signals.ds.readers.influx_injector.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  influx-api-proxy:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.services.timeseries.influx -n miimetiq.workers.services.timeseries.influx.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf
  {{- end }}

  dashboards-datasource:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
      - graphite-database-data:/opt/miimetiq/env/opt/graphite/storage/whisper
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.services.dashboards.worker -n miimetiq.workers.services.dashboards.worker.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  SimpleDS:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.services.ds.signal_info -n miimetiq.workers.services.ds.signal_info.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -c100 -P eventlet
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  writers-sender:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.services.ds.writer_send -n miimetiq.workers.services.ds.writer_send.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -c100 -P eventlet
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  assets-history-recorder:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.signals.ds.readers.historic -n miimetiq.workers.signals.ds.readers.historic.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  writers-QoS-2:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.signals.ds.readers.online_writers_delivery -n miimetiq.workers.signals.ds.readers.online_writers_delivery.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  assets-authorization-manager:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.signals.models.asset_exchange_manager -n miimetiq.workers.signals.models.asset_exchange_manager.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  assets-connection-status:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    command: wait-for-config.sh /usr/local/bin/python -c "from miimetiq.workers.connection_instrument import run; run()"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  license-client:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-crt:/opt/miimetiq/data
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/python -c "from miimetiq.workers.client import build_client; build_client()"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  signals-authorization-manager:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    environment:
      DEBUG_GRAPHS: false
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/python -c "from miimetiq.workers.signals.models.signals_authorization_manager import worker; worker.run()"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  configuration-executor:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.scheduler -n miimetiq.workers.scheduler.vm.miimetiq.local
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  configuration-scheduler:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.signals.models.update_schedule_configuration_tasks -n miimetiq.workers.update_schedule_configuration_tasks.vm.miimetiq.local
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

  license-import:
    image: nexiona/miimetiq-core-services:2.0.0-alpha8
    volumes:
      - miimetiq-crt:/opt/miimetiq/data
      - miimetiq-schemas:/opt/miimetiq/schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /usr/local/bin/celery worker -A miimetiq.workers.services.license.export_import -n miimetiq.workers.export_import.vm.miimetiq.local
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}
    metadata: *miimetiq-conf

volumes:
  database-data:
  influx-database-data:
  message-broker-data:
  graphite-database-data:
  miimetiq-crt:
  miimetiq-schemas:
