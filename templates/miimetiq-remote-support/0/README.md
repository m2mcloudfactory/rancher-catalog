# MIIMETIQ Remote Support

MIIMETIQ Remote Support enables a secure access to an EDGE device for remote support by tunneling SSH connections through SSL.

To activate the service just start the stack. Once you have received the remote support you can disable it by stopping the stack.
