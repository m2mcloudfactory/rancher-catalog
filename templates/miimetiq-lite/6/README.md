# MIIMETIQ LITE

MIIMETIQ® LITE is a lightweight multi-tenant IoT platform builder with a pre-defined but customizable User Interface based on MIIMETIQ® IoT technology.

## Deployment
* Select MIIMETIQ LITE from the community catalog
* Answer all the questions
* Click deploy

## Usage
* MIIMETIQ LITE is available on port 80. The default credentials are amAdmin/miimetiqadmin

## CHANGELOG

### 2.0.5
* Added option to launch FlexibleDS with priviledged permissions upon host devices
* User manual is now available in the MIIMETIQ LITE UI.
* Upgraded docker images to Ubuntu 16.04
