version: '2'
services:
  database:
    image: nexiona/database:2.1.3
    volumes:
      - database-data:/var/lib/mongodb
    restart: always
    command: /usr/bin/mongod --dbpath /var/lib/mongodb
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:host_label_soft: io.nexiona.host.type=dedicated

  email-relay:
    image: namshi/smtp
    environment:
      SMARTHOST_ADDRESS: $SMTP_ADDRESS
      SMARTHOST_PORT: $SMTP_PORT
      SMARTHOST_USER: $SMTP_USER
      SMARTHOST_PASSWORD: $SMTP_PASSWORD
      SMARTHOST_ALIASES: "*"
    restart: always
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  message-broker:
    image: nexiona/message-broker:2.1.3
    environment:
      RABBITMQ_SERVER_START_ARGS: "-eval error_logger:tty(true)"
    ports:
      - "5672:5672"
    volumes:
      - message-broker-data:/var/lib/rabbitmq
    restart: always
    command: wait-for-config.sh /usr/lib/rabbitmq/bin/rabbitmq-server
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:host_label_soft: io.nexiona.host.type=dedicated

  API-core:
    image: nexiona/core-services:2.1.3
    environment:
      PYTHONPATH: /opt/miimetiq/git
    volumes:
      - miimetiq-crt:/opt/miimetiq/data
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh wait-for-it.sh message-broker:5672 --timeout=30 --strict -- uwsgi -s :7000 --workers 4 --enable-threads --threads 4 --wsgi-file /opt/miimetiq/git/bin/uwsgi.py --pidfile /var/run/uwsgi.pid --die-on-term -M --buffer-size=8192 --need-app
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  webserver:
    image: nexiona/webserver:2.1.3
    environment:
      LD_LIBRARY_PATH: /opt/miimetiq/env/opt/nginx/luajit/lib:/opt/miimetiq/env/opt/nginx/lualib
    ports:
      - "80:80"
      - "443:443"
    restart: always
    command: wait-for-config.sh /usr/local/bin/nginx-with-basic-auth.sh miimetiq "${DOCS_SITE_PASSWORD}"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  metrics-database:
    image: nexiona/metrics-database:2.1.3
    volumes:
      - metrics-database-data:/opt/graphite/storage/whisper
    restart: always
    working_dir: /opt/graphite
    command: wait-for-config.sh /usr/local/bin/start-carbon.sh --debug
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  MQTT-server:
    image: nexiona/mqtt-server:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    ports:
      - "1883:1883"
    restart: always
    command: wait-for-config.sh /usr/bin/node /opt/miimetiq/git/utils/mqtt_cl/mqtt_cl.js
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  rules-engine:
    image: nexiona/rules-engine:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    working_dir: /opt/miimetiq/git/miimetiq/workers/signals/models/rules_backend
    command: wait-for-config.sh node rules_worker.js
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  readers-injector:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.ds.reader.injector -n miimetiq.workers.ds.reader.injector.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  metrics-injector:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.ds.reader.metrics.graphite_injector -n miimetiq.workers.ds.reader.metrics.graphite_injector.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  dashboards-datasource:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
      - metrics-database-data:/opt/miimetiq/env/opt/graphite/storage/whisper
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.services.dashboards.worker -n miimetiq.workers.services.dashboards.worker.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  SimpleDS:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.services.ds.signal_info -n miimetiq.workers.services.ds.signal_info.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -c100 -P eventlet
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  writers-sender:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.services.ds.writer_send -n miimetiq.workers.services.ds.writer_send.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -c100 -P eventlet
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  metrics-reader-timeseries-API:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
      - metrics-database-data:/opt/miimetiq/env/opt/graphite/storage/whisper
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.services.graphite.metrics_proxy -n miimetiq.workers.services.graphite.metrics_proxy.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  assets-history-recorder:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.signals.ds.readers.historic -n miimetiq.workers.signals.ds.readers.historic.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  writers-QoS-2:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.signals.ds.readers.online_writers_delivery -n miimetiq.workers.signals.ds.readers.online_writers_delivery.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  assets-authorization-manager:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.signals.models.asset_exchange_manager -n miimetiq.workers.signals.models.asset_exchange_manager.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  assets-connection-status:
    image: nexiona/core-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    command: wait-for-config.sh /root/.pyenv/shims/python -- "/opt/miimetiq/git/miimetiq/workers/connection_instrument.pyc"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  license-client:
    image: nexiona/core-services:2.1.3
    volumes:
      - miimetiq-crt:/opt/miimetiq/data
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    working_dir: /opt/miimetiq/git
    command: wait-for-config.sh /root/.pyenv/shims/python "/opt/miimetiq/git/miimetiq/mmn_client/client.pyc"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  # license-import:
  #  image: nexiona/core-services:2.1.3
  #  volumes:
  #    - miimetiq-crt:/opt/miimetiq/data
  #    - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
  #  restart: always
  #  working_dir: /opt/miimetiq/git
  #  command: wait-for-config.sh /root/.pyenv/shims/celery worker -A miimetiq.workers.services.license.export_import -n miimetiq.workers.services.license.export_import.vm.miimetiq.local --without-gossip --without-mingle --without-heartbeat -P solo
  #  labels:
  #    io.rancher.container.hostname_override: container_name
  #    io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  duplicate-devices-API:
    image: nexiona/lite-services:2.1.3
    restart: always
    working_dir: /opt/miimetiq-lite/src/services
    command: wait-for-config.sh /opt/miimetiq-lite/src/services/rpc_duplicate_asset
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  FlexibleDS-management-API:
    image: nexiona/lite-services:2.1.3
    restart: always
    working_dir: /opt/miimetiq-lite/src/services
    command: wait-for-config.sh /opt/miimetiq-lite/src/services/rpc_node_manager
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  tenants-model-manager-API:
    image: nexiona/lite-services:2.1.3
    restart: always
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    working_dir: /opt/miimetiq-lite/src/services
    command: wait-for-config.sh /opt/miimetiq-lite/src/services/rpc_schema_services
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  tenants-metrics-recorder:
    image: nexiona/lite-services:2.1.3
    restart: always
    working_dir: /opt/miimetiq-lite/src/services
    command: wait-for-config.sh /opt/miimetiq-lite/src/services/tenant_model_listener
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  tenants-manager-API:
    image: nexiona/lite-services:2.1.3
    volumes:
      - tenant-schemas:/opt/miimetiq-lite/tenant-schemas
    restart: always
    working_dir: /opt/miimetiq-lite/src/services
    command: wait-for-config.sh /opt/miimetiq-lite/src/services/tenants_handler
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  FlexibleDS-manager:
    image: nexiona/lite-services:2.1.3
    restart: always
    working_dir: /opt/miimetiq-lite/src/services
    command: wait-for-config.sh /opt/miimetiq-lite/src/services/signal_node_manager
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.container.create_agent: true
      io.rancher.container.agent.role: environment
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

  # tenant-creator:
  #   image: nexiona/tenant-creator:2.1.3
  #   command: wait-for-license.sh python -u .
  #   restart: "on-failure"
  #   volumes:
  #       - tenant-creator-data:/var/cache/tenant-creator
  #   labels:
  #     io.rancher.container.start_once: true
  #     io.rancher.container.hostname_override: container_name
  #     io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack.name=$${stack_name}

volumes:
  miimetiq-crt:
  metrics-database-data:
  message-broker-data:
  tenant-schemas:
  database-data:
  tenant-creator-data:
