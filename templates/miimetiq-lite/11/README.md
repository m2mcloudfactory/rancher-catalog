# MIIMETIQ LITE

MIIMETIQ® LITE is a lightweight multi-tenant IoT platform builder with a pre-defined but customizable User Interface based on MIIMETIQ® IoT technology.

### Deployment
* Select MIIMETIQ LITE from the community catalog
* Answer all the questions
* Click deploy

### Usage
* MIIMETIQ LITE is available on port 80. The default credentials are amAdmin/miimetiqadmin
