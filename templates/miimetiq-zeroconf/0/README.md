# MIIMETIQ Zeroconf

MIIMETIQ Zeroconf is a mDNS/DNS-SD service for Rancher to be used in EDGE devices for:

1. Discovering the IP of the device
2. Connecting directly to the Rancher interface and deploying new services from the catalog
3. Configuring extra host aliases which are required in MIIMETIQ products such as COMPOSER and LITE

This service will resolve multicast DNS queries asking for the hostname of the device to the local IP address. For instance, if the hostname is _miimetiq-edge_ and the IP address is _192.168.1.100_, any multicast DNS query asking for the IP address of _miimetiq-edge.local_ will be answered by the service pointing to _192.168.1.100_.
