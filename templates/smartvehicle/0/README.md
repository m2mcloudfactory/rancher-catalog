# MIIMETIQ Smart Vehicle

MIIMETIQ® Smart Vehicle is a platform for Smart Vehicles.

### Requirements
* A MIIMETIQ LITE already deployed in the same machine

### Deployment
* Select MIIMETIQ Smart Vehicle from the community catalog
* Answer all the questions
* Click deploy

### Usage
* MIIMETIQ Smart Vehicle is available on port 80. No credentials are required.
